# 9329-fingrp3-javalanguage

### Members
<ul>
    <li>Bernabe, Rabelais Mar</li>
    <li>Cortez, Leo Anthony</li>
    <li>de Guzman, Alastair Zeph</li>
    <li>Dongga-as, Saleena Marie</li>
    <li>Ecleo, Kent John</li>
    <li>Pereira, Fiona</li>
</ul>

## Running the programs

### Java Server
1. Open a terminal or cmd and go to `<project directory>\out\prodction\9329-fingrp3-`
2. Run the orbd using the command `orbd -ORBInitialHost <host> -ORBInitialPort <port>` where <host> is the IP address of the server and <port> is the port number to be opened.
3. Start the Java server by first going to Server_Java directory `<project directory>\out\prodction\9329-fingrp3\Server_Java\` . Now you are in the Java_Server directory, type the `java -cp ..\..\..\..\server_lib\mysql-connector-java-8.0.29.jar; server.Server`. If the Server does not run try `java -cp ..\..\..\..\server_lib\mysql-connector-java-8.0.29.jar; server.Server -ORBInitialHost <host> -ORBInitialPort <port>` or `start java -cp ..\..\..\..\server_lib\mysql-connector-java-8.0.29.jar; server.Server -ORBInitialHost <host> -ORBInitialPort <port>`.
5. Press enter.
6. The Server will ask for the username and password of the mysql database.

### Java Client
1. Open the terminal and go to `<project directory>\out\prodction\9329-fingrp3\Client_Java\`.
2. Now you are in the Client_Java directory, type the `java client.App`. If the Client does not run try `java client.App -ORBInitialHost <host> -ORBInitialPort <port>` or `start java client.App -ORBInitialHost <host> -ORBInitialPort <port>`
3. Press enter.

### Python Client
1. Open the terminal and go to `<project directory>\src\Client_Python\`. Set the path by `set PYTHONPATH=<OMNIORB path>\lib\python;<OMNIORB path>\lib\x86_win32`
2. Now you are in the Client_Python directory, type the `python ClientApp.py`. If the client does not run try typing `python ClientApp.py -python PythonClient.py -ORBInitRef NameService=corbaname::<host>:<port>`
	