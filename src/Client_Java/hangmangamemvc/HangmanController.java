package hangmangamemvc;

import hangmangame.HangmanPackage.GameOverException;
import hangmangame.HangmanPackage.SameLetterInputException;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.ConnectException;

public class HangmanController {

    private HangmanView view;
    private final String username;
    private Result resDisplay;

    public HangmanController(String username){
        view = new HangmanView();
        this.username = username;
        resDisplay = new Result();
        requestWord();
        checkAnswer();
        closingWindow();
    }

    public void requestWord(){
        int lenWord = HangmanModel.hangman.requestWord(username);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < lenWord; i++){
            sb.append("_ ");
        }
        view.setFlashWord(sb.toString().trim());
    }

    public void checkAnswer(){
        view.setEnterButtonListener(e -> {
            String entry = view.getEntry().trim();
            try {
                if (entry.length() > 1 ||
                        !Character.isLetter(entry.charAt(0)) && entry.equals("-")
                ){
                    view.displayError("wrong entry");
                    return;
                }
            } catch (StringIndexOutOfBoundsException ee){
                view.displayError("Enter a letter");
                return;
            }
            boolean res = false;
            try {
                res = HangmanModel.hangman.checkAnswer(username, entry.charAt(0));
            }catch (org.omg.CORBA.COMM_FAILURE comm_failure){
                JOptionPane.showMessageDialog(null, "Loss connection to server. The game will close",
                        "Error", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            } catch (SameLetterInputException ex) {
//                ex.printStackTrace();
                incrementMistake();
            } catch (GameOverException gameOverException) {
//                gameOverException.printStackTrace();
                resDisplay.setVisible(true);
                resDisplay.setSize(400, 300);
                resDisplay.pack();
                resDisplay.setResField("Game Over");
                resDisplay.setMsgLabel("Better luck next time. The word is "
                        + HangmanModel.hangman.getTheGuessWord(username));
                resPlayagainBtn();
                resQuitBtn();
            }
            if (!res){
                    incrementMistake();
                    return;
            }
            int [] indexes = HangmanModel.hangman.updateFlashedWord(username, entry.charAt(0));
            updateFlashWord(indexes, entry.charAt(0));
            isMatch();
        });
    }

    private void isMatch(){
        String[] token = view.getFlashWord().trim().split(" ");
        StringBuilder sb = new StringBuilder();
        for (String s : token)
            sb.append(s);
        boolean res = HangmanModel.hangman.isMatch(username, sb.toString());
        if (res){
            resDisplay.setVisible(true);
            resDisplay.setSize(400, 300);
            resDisplay.pack();
            resDisplay.setResField("Congratulations");
            resDisplay.setMsgLabel("You got the correct word  "
                    + HangmanModel.hangman.getTheGuessWord(username));
            resPlayagainBtn();
            resQuitBtn();
        }

    }

    private void resPlayagainBtn(){
        resDisplay.setPlayagainBtnListener(e1 ->{
            // todo play again
            playAgain();
            resDisplay.dispose();
        });
    }

    private void resQuitBtn(){
        resDisplay.setQuitBtnListener(e2 ->{
            HangmanModel.hangman.stopGame(username);
            view.dispose();
            resDisplay.dispose();
        });
    }

    public void updateFlashWord(int[] i, char letter){
        String[] token = view.getFlashWord().split(" ");
        // replace the indexes with letter
        for (int j : i){
            token[j] = String.valueOf(letter);
        }
        StringBuilder sb = new StringBuilder();
        for (String s : token) {
            sb.append(s).append(" ");
        }
        view.setFlashWord(sb.toString());
    }

    public void playAgain(){
        view.clearTextFields();
        int lenWord = HangmanModel.hangman.playAgain(username);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < lenWord; i++){
            sb.append("_ ");
        }
        view.setFlashWord(sb.toString().trim());
    }

    private void incrementMistake() {
        view.setNumMistake(HangmanModel.hangman.getNumberOfMistake(username));
    }

    private void closingWindow(){
        // when the user close the game without quiting
        view.windowClosingListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    HangmanModel.hangman.stopGame(username);
                } catch (org.omg.CORBA.COMM_FAILURE comm_failure){
                    JOptionPane.showMessageDialog(null, "Loss connection to server. The game will shutdown", "Error", JOptionPane.ERROR_MESSAGE);
                    System.exit(0);
                }

            }
        });
    }

}