package hangmangamemvc;

import javax.swing.*;
import java.awt.event.*;
import java.util.Locale;

public class Result extends JDialog {
    private JPanel contentPane;
    private JButton playagainBtn;
    private JButton quitBtn;
    private JPanel downPanel;
    private JPanel upperPanel;
    private JPanel btnPanel;
    private JTextField resField;
    private JLabel msgLabel;

    public Result() {
        setContentPane(contentPane);
        getRootPane().setDefaultButton(playagainBtn);

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    public void setResField(String msg){
        resField.setText(msg.trim().toUpperCase(Locale.ROOT));
    }

    public void setMsgLabel(String msg){
        msgLabel.setText(msg);
    }

    public void setPlayagainBtnListener(ActionListener e){
        playagainBtn.addActionListener(e);
    }

    public void setQuitBtnListener(ActionListener e){
        quitBtn.addActionListener(e);
    }
}
