package hangmangamemvc;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class HangmanView extends JFrame{
    private JPanel mainPanel;
    private JLabel headerLabel;
    private JTextField flashWordField;
    private JTextField entryField;
    private JLabel entryLabel;
    private JLabel numMistakeLabel;
    private JTextField mistakeField;
    private JButton enterButton;

    public HangmanView(){
        createFrame();
    }

    public void setFlashWord(String word){
        flashWordField.setText(word.trim());
    }

    public String getFlashWord(){
        return flashWordField.getText().trim();
    }

    public void setNumMistake(int m){
        mistakeField.setText(String.valueOf(m));
    }

    public void setEnterButtonListener(ActionListener e){
        enterButton.addActionListener(e);
    }

    public String getEntry(){
        return entryField.getText();
    }

    public void displayError(String msg){
        JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public void clearTextFields(){
        entryField.setText("");
        mistakeField.setText("");
    }

    public void windowClosingListener(WindowAdapter e){
        addWindowListener(e);
    }

    private void createFrame(){
        this.setContentPane(mainPanel);
        this.setTitle("Hangman Game");
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(500, 300);
        this.setResizable(false);
    }

}
