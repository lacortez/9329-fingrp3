package client;

import hangmangamemvc.HangmanController;
import loginmvc.LoginController;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;

import java.io.IOException;

public class MainController {

    public MainController(String[] args){
        try {
            Connector connector = new Connector(args);
            LoginController login = new LoginController();
            login.loginBtnListener();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidName e) {
            e.printStackTrace();
        } catch (org.omg.CosNaming.NamingContextPackage.InvalidName e) {
            e.printStackTrace();
        } catch (CannotProceed e) {
            e.printStackTrace();
        } catch (NotFound e) {
            e.printStackTrace();
        }

    }
}
