package client;

import hangmangame.*;
import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class Connector {

    private Hangman hangman;
    private loginmvc.LoginModel loginModel;
    private hangmangamemvc.HangmanModel hangmanModel;

    public Connector(String[] args) throws IOException, InvalidName, org.omg.CosNaming.NamingContextPackage.InvalidName, CannotProceed, NotFound {
        String[] param = readConfig();
        ORB orb;
        if(args.length > 1)
            orb = ORB.init(args, null);
        else
            orb = ORB.init(param, null);

        org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");

        NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

        String name = "hangmanGame";
        hangman = HangmanHelper.narrow(ncRef.resolve_str(name));

        loginModel.login = hangman;
        hangmanModel.hangman = hangman;

    }

    private static String[] readConfig() throws IOException {
        StringBuilder cmd = new StringBuilder();
        String[] config = null;
        // read the configuration for the client
        FileInputStream file = new FileInputStream("../../../../config.properties");
        Properties pr = new Properties();
        pr.load(file);

        String host = pr.getProperty("host");
        String port = pr.getProperty("port");

        cmd.append(" -ORBInitial ").append(host);
        cmd.append(" -ORBInitialPort ").append(port);

        config = cmd.toString().split("\\s");
        return config;
    }


}
