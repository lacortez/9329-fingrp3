package loginmvc;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.Arrays;

public class LoginView extends JFrame{
    private JPasswordField passwordField;
    private JPanel mainPanel;
    private JTextField usernameField;
    private JButton loginBtn;
    private JLabel passwordLabel;
    private JLabel usernameLabel;

    public LoginView(){
        super();
        setContentPane(mainPanel);
        setSize(600, 404);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    public String getUsername(){
        return usernameField.getText();
    }

    public String getPassword() {
        StringBuilder sb = new StringBuilder();
        for (int i =0; i < passwordField.getPassword().length; ++i){
            sb.append(passwordField.getPassword()[i]);
        }
        return sb.toString();
    }

    public void loginBtnListener(ActionListener listenerForLogInButton){
        loginBtn.addActionListener(listenerForLogInButton);
    }

    public void showErrorMessage(String msg){
        JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
    }



}