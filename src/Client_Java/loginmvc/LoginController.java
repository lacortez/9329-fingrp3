package loginmvc;


import hangmangamemvc.HangmanController;
import hangmangamemvc.HangmanModel;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class LoginController{

    private LoginView view;
    private LoginModel model;
    private boolean res;

    public LoginController(){
        // initialize model and view attributes
        this.view = new loginmvc.LoginView();
        view.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    HangmanModel.hangman.stopGame(view.getUsername());
                } catch (org.omg.CORBA.COMM_FAILURE comm_failure){
                    JOptionPane.showMessageDialog(null, "Loss connection to server. The game will shutdown", "Error", JOptionPane.ERROR_MESSAGE);
                    System.exit(0);
                }
            }
        });
    }

    public boolean clientLogin(){
        return res;
    }

    public void loginBtnListener(){
        view.loginBtnListener(e ->{
            // get the credentials
            String username = view.getUsername();
            String password = view.getPassword();
            if (username == "" || password == ""){
                JOptionPane.showMessageDialog(null, "Warning",
                        "Please fill the test fields", JOptionPane.WARNING_MESSAGE);
                return;
            }
            try{
                res = LoginModel.login.clientLogin(username, password);
            } catch (org.omg.CORBA.COMM_FAILURE comm_failure) {
                JOptionPane.showMessageDialog(null, "Loss connection to server. The game will close",
                        "Error", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }
            if (res){
                // TODO: HangmanController constructor to accept the username
                HangmanController hangman = new HangmanController(getUsername());
                disposeLogin();
            }else{
                displayErrorMessage("Wrong username or password.\n" +
                        "Please try again.");
            }
        });
    }

    public String getUsername(){
        return view.getUsername();
    }

    public void displayErrorMessage(String msg){
        view.showErrorMessage(msg);
    }

    public void disposeLogin(){
        view.dispose();
    }

}