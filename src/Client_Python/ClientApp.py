import sys
from tkinter import DISABLED, Button, Entry, Frame, Label, Tk, messagebox
from tkinter.font import NORMAL
from omniORB import CORBA

import hangmangame
from connector import Connector

username = ''
mistakes = 0


con = Connector(sys.argv)

def display_frame(frame):
    frame.tkraise()

def display_error_msg(mesg, prt):
	error = messagebox.showerror('Error', mesg, parent=prt)

def display_message(mesg, prt):
    messsage = messagebox.showinfo('Information', mesg, parent=prt)

root = Tk()
root.title('Hangman Game')

root.rowconfigure(0, weight=1)
root.columnconfigure(0, weight=1)

login = Frame(root)
hangman = Frame(root)
result = Frame(root)

for fr in (login, hangman, result):
    fr.grid(row=0, column=0, sticky='nsew')

display_frame(login)


#=========== LOGIN FRAME
def login_btn_click():
    res = True
    global username
    username = username_entry.get()
    password = password_entry.get()

    if username=='' and password=='':
        display_error_msg('Fill in the text fields', login)
        return
    try:
        res = con.hangman.clientLogin(username, password)
    except CORBA.TRANSIENT as x:
        display_error_msg("There is no connection to the server.\nTry again later", login)
        return
    if not res:
        display_message('Wrong username or password', login)
        return
    request_word()
    display_frame(hangman)

# create the label for username
username_label = Label(login, text="Username: ", font="Arial 10 bold")
username_label.grid(row=0, column=0, padx=(30, 10), pady=(30, 20))
# entry for username
username_entry = Entry(login)
username_entry.grid(row=0, column=1, padx=(0, 10), pady=(30, 20))
# create label for password
password_label = Label(login, text="Password: ", font="Arial 10 bold")
password_label.grid(row=1, column=0, padx=(30, 10), pady=10)
#create password entry
password_entry = Entry(login, show='*')
password_entry.grid(row=1, column=1, padx=(0, 10), pady=(30, 20))
#create login button
login_btn = Button(login, text="Login", command=login_btn_click)
login_btn.grid(row=2, column=1)

#========== HANGMANGAME FRAME
len_of_word = 0

index_of_guess_letter = []


def request_word():
    global username

    len_of_word = con.hangman.requestWord(username)
    num = 5
    guess_word = ''
    for i in range(num):
        guess_word += "_ "
    flash_word.config(state=NORMAL)
    flash_word.delete(0, 'end')
    flash_word.insert(0, guess_word)
    flash_word.config(state=DISABLED)

def play_again():
    global username
    try:
        update_number_mistake()
        gl_entry.delete(0, 'end')
        len_of_word = con.hangman.playAgain(username)
    except CORBA.TRANSIENT as ct:
        display_error_msg("There is no connection to the server.\nTry again later", login)
        return
    guess_word = ''
    for i in range(len_of_word):
        guess_word += "_ "
    flash_word.config(state=NORMAL)
    flash_word.delete(0, 'end')
    flash_word.insert(0, guess_word)
    flash_word.config(state=DISABLED)

def update_flash_word(index, letter):
    try:
        fw = flash_word.get().split()
        for i in index:
            try:
                fw[i] = letter
            except IndexError as x:
                try:
                    e = len(fw) - (len(fw) - i)
                    fw[e] = letter
                except IndexError:
                    pass
        flash_word.config(state=NORMAL)
        flash_word.delete(0, 'end')
        flash_word.insert(0, ' '.join(fw))
        flash_word.config(state=DISABLED)
    except CORBA.BAD_PARAM as bp:
        pass


def update_number_mistake():
    global mistakes

    mistakes = con.hangman.getNumberOfMistake(username)
    mt_label.config(text="Number of mistakes: {mistake}".format(mistake=mistakes))


def check_is_match():
    global username
    curr_word = ''.join(flash_word.get().split())
    res = con.hangman.isMatch(username, curr_word)
    if res:
        match_word("CONGRATULATIONS", "'Good Job! You're able to get the word.")
        flash_word.config(state=NORMAL)
        flash_word.delete(0, 'end')
        gl_entry.delete(0, 'end')
        display_frame(result)

def check_answer():
    global username
    curr_input = gl_entry.get()
    # validate the user input
    if len(curr_input) > 1 or not curr_input.isalpha() and curr_input != '-':
        display_error_msg("Incorrect input", hangman)
        gl_entry.delete(0, 'end')
        return

    try:
        res = con.hangman.checkAnswer(username, curr_input)
    except hangmangame.Hangman.SameLetterInputException as sm:
        update_number_mistake()
        pass
    except hangmangame.Hangman.GameOverException as go:
        match_word("Game Over", "Better luck next time. Your word is {word}".format(word=con.hangman.getTheGuessWord(username)))
        flash_word.config(state=NORMAL)
        flash_word.delete(0, 'end')
        gl_entry.delete(0, 'end')
        display_frame(result)
        pass
    except CORBA.TRANSIENT as co:
        display_error_msg("There is no connection to the server.\nTry again later", login)
        pass
    try:
        if not res:
            update_number_mistake()
            pass
    except UnboundLocalError:
        pass
    gl_entry.delete(0,'end')
    # get the position/s of the guess letter in the guess word
    index_of_guess_letter = con.hangman.updateFlashedWord(username, curr_input)
    update_flash_word(index_of_guess_letter, curr_input)
    check_is_match()

#create title label
title_label = Label(hangman, text='Guess the word', font="Arial 20 bold")
title_label.pack()
#create entry to display the guess word
flash_word = Entry(hangman, width=30, font="Arial 25 bold", justify='center', borderwidth=1)
flash_word.pack()
# create label for where to enter the letter
gl_label = Label(hangman, text="Enter your guess letter here", font="Arial 10 bold")
gl_label.pack(pady=(30, 10))
# create entry to enter the guess letter
gl_entry = Entry(hangman, width=10, font="Arial 15 bold", justify='center')
gl_entry.pack()
# button to enter the guess letter
gl_btn = Button(hangman, text="Enter", command=check_answer)
gl_btn.pack(pady=(10, 15))
#label for mistakes
mt_label = Label(hangman, text="Number of mistakes: {mistake}".format(mistake=mistakes), font="Arial 10 bold")
mt_label.pack()


#================ RESULT FRAME
def play_again_btn_click():
    display_frame(hangman)
    play_again()

def quit_btn_click():
    global root
    root.destroy()

def match_word(res, msg):
    res_label.config(text=res)
    msg_label.config(text=msg)


#label for result (congratulations | better luck next time)
res_label = Label(result, font="Arial 30 bold", text="BETTER LUCK NEXT TIME")
res_label.pack(pady=(30, 10))
# label for the message to the client
msg_label = Label(result, font='Arial 15 bold', text='You are unable to get the guess word.\nThe guess word is')
msg_label.pack(pady=10)
# button to play again
play_again_btn = Button(result, text='Play again', command=play_again_btn_click)
play_again_btn.pack(pady=10)
# quit game btn
quit_btn = Button(result, text='Quit', command=quit_btn_click, bg='red')
quit_btn.pack()

def on_closing():
    global username
    con.hangman.stopGame(username)
    root.destroy()

root.geometry('700x300')
root.resizable(False, False)
root.wm_protocol("WM_DELETE_WINDOW", on_closing)

root.mainloop()
