import sys
from omniORB import CORBA
import CosNaming, hangmangame

from configparser import ConfigParser

class Connector:

    def __init__(self, args):
        self.read_config()
        # initialise orb object
        if len(args) > 1:
            self.orb = CORBA.ORB_init(args, CORBA.ORB_ID)
        elif len(self.args) > 1:
            self.orb = CORBA.ORB_init(self.args, CORBA.ORB_ID)

        self.obj = self.orb.resolve_initial_references("NameService")
        self.rootContext = self.obj._narrow(CosNaming.NamingContext)

        if self.rootContext is None:
            print("Failed to narrow the root naming context")
            sys.exit(1)

        self.name = [CosNaming.NameComponent('hangmanGame', '')]

        try:
            self.obj = self.rootContext.resolve(self.name)
        except CosNaming.NamingContext.NotFound as ex:
            print("name not found")
            sys.exit(1)
            
        self.hangman = self.obj._narrow(hangmangame.Hangman)

    def read_config(self):
        self.args = []

        # read the configuration file
        config = ConfigParser()
        config.read('config.ini')

        host = config.get('server', 'host')
        port = config.get('server', 'port')

        self.args = ['connector.py', '-ORBInitRef', 'NameService=corbaname::{host}:{port}'.format(host=host, port=port)]
        