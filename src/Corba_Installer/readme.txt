HOW TO INSTALL PYTHON IN WINDOWS
	1. Download the python installer in their website. https://www.python.org/downloads/windows/
	2. Under the “Python Releases for Windows” heading, click the link for the Latest Python
	Release - Python 3.x.x. As of this writing, the latest version was Python 3.10.4.
	3. Scroll to the bottom and select either Windows x86-64 executable installer for 64-bit or Windows x86 executable installer for 32-bit. 
	NOTE: Download the python installer according to the architecture of your device
	4. After successfuly downloading the python installer, run the python installer
	5. For now we would choose the 'Install Now' as it includes the neccessary components for python
	6. The Add Python 3.10 to PATH checkbox is unchecked by default. You can add manualy after installing the python to the PATH.