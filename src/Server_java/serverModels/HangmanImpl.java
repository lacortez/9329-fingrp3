package serverModels;

import hangmangame.HangmanPOA;
import hangmangame.HangmanPackage.GameOverException;
import hangmangame.HangmanPackage.SameLetterInputException;
import server.Server;


import java.util.ArrayList;
import java.util.List;

public class HangmanImpl extends HangmanPOA {
    private DbUtils dbUtil;

    @Override
    public boolean clientLogin(String username, String password) {
        if(Server.userUsedWords.containsKey(username)) return false;
        boolean res = dbUtil.clientLoggingIn(username, password);
        if (res){
            Server.userCurrWord.put(username, "");
            Server.userUsedWords.put(username, Server.setWord(username, ""));
            Server.userMistakes.put(username, 0);
            Server.userInputChar.put(username, Server.setUsedChar(username, '\0'));
        }
        return res;
    }

    @Override
    public int requestWord(String username) {
        boolean res = true;
        String mysteryWord;
        // check if the word got is not used
        do{
            mysteryWord = new WordPicker().getRandomWord();
            res = isUsed(username, mysteryWord);
        }while(res);
        Server.userCurrWord.put(username, mysteryWord);
        Server.userUsedWords.put(username, Server.setWord(username, mysteryWord));
        return mysteryWord.length();
    }

    private boolean isUsed(String username, String word){
        boolean res = false;
        res = Server.userUsedWords.get(username).contains(word);
        return res;
    }

    @Override
    public int getNumberOfMistake(String username) {
        return Server.userMistakes.get(username);
    }

    @Override
    public boolean checkAnswer(String username, char letter) throws SameLetterInputException, GameOverException {
        boolean res = false;
        // get the curr word for the user
        String currWord = Server.userCurrWord.get(username);
        // check if the letter is already entered
        if (Server.userInputChar.get(username).contains(letter)) {
            // increment the number of mistakes by the user
            int currMistake = Server.userMistakes.get(username);
            Server.userMistakes.put(username, currMistake + 1);
            if (Server.userMistakes.get(username) >= 5) throw new GameOverException("Game over");
            throw new SameLetterInputException("same letter");
        }
        Server.userInputChar.put(username, Server.setUsedChar(username, letter));
        // check if the currWord contains the input letter
        res = currWord.trim().contains(String.valueOf(letter));
        if (!res) {
            // increment the number of mistakes by the user
            // when the current word does not contain the input letter
            int currMistake = Server.userMistakes.get(username);
            Server.userMistakes.put(username, currMistake + 1);
        }
        // when the user richest 5 mist it will throw the GameOverException
        if (Server.userMistakes.get(username) >= 5) throw new GameOverException("game over");
        return res;
    }

    @Override
    public int[] updateFlashedWord(String username, char letter) {
        List<Integer> indexes = new ArrayList<>();
        // get the curr word for the user
        String currWord = Server.userCurrWord.get(username);
        for (int i = 0; i < currWord.length(); i++){
            if (currWord.charAt(i) == letter)
                indexes.add(i);
        }
        int[] res = new int[indexes.size()];
        for(int j = 0; j < indexes.size(); j++){
            res[j] = indexes.get(j);
        }
        return res;
    }

    @Override
    public boolean isMatch(String username, String word) {
        return Server.userCurrWord.get(username).equals(word.trim());
    }

    @Override
    public String getTheGuessWord(String username) {
        return Server.userCurrWord.get(username).trim();
    }

    @Override
    public int playAgain(String username) {
        // reset number of mistake and used character
        Server.userMistakes.remove(username);
        Server.userMistakes.put(username, 0);
        Server.userInputChar.remove(username);
        Server.userInputChar.put(username, Server.setUsedChar(username, '\0'));
        return requestWord(username);
    }

    @Override
    public boolean stopGame(String username) {
        Server.userCurrWord.remove(username);
        Server.userMistakes.remove(username);
        Server.userUsedWords.remove(username);
        Server.userInputChar.remove(username);
        return true;
    }

    //database setup
     public void setDbUtil(DbUtils dbUtil){
        this.dbUtil = dbUtil;
     }
}
