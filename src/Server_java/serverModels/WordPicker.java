package serverModels;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WordPicker {
    private List<String> words = new ArrayList<>();
    private BufferedReader fileRdr;

    //test
    /*
    public static void main(String[] args){
        for(int x = 0; x < 5; x++){
            System.out.println(new serverModels.WordPicker().getRandomWord());
        }
    }*/

    WordPicker() {
        try {
            String currentLine;
            fileRdr = new BufferedReader(new FileReader("../../../../server_lib/words.txt"));
            while((currentLine = fileRdr.readLine()) != null){
                words.add(currentLine);
            }
        } catch (IOException ioex) {
            ioex.printStackTrace();
        } finally {
            try{
                if (fileRdr != null)
                    fileRdr.close();
            } catch (IOException ioex){
                ioex.printStackTrace();
            }
        }
    }

    public String getRandomWord() {
        Random picker = new Random();
        int i = picker.nextInt(words.size());

        return words.get(i);
    }


}
