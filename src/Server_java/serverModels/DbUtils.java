package serverModels;

import java.sql.*;

public class DbUtils {
    private String address = "jdbc:mysql://localhost:3306/finproj";
    private String user;
    private String pass;
    private Connection cn = null;


    public DbUtils() {}

    public void setUser(String user){
        this.user = user;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void connectToDb() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        cn = DriverManager.getConnection(address, user, pass);
        //double check later please and thank you
    }

    public void closeConnectionToDb(){
        if (cn != null) {
            try {
                cn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean clientLoggingIn(String clientUname, String clientPass){
        PreparedStatement preppedStatement = null;
        ResultSet resultSet = null;
        try{
            preppedStatement = cn.prepareStatement("SELECT password FROM users WHERE username = ?");
            preppedStatement.setString(1, clientUname);
            resultSet = preppedStatement.executeQuery();

            if(!resultSet.isBeforeFirst()){
                return false;
            }
            resultSet.next();
            String retrievedClntPass = resultSet.getString("password");
            if(retrievedClntPass.equals(clientPass)){
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    public String psToString(char[] ps) {
        StringBuilder sb = new StringBuilder();
        for (char s : ps) {
            sb.append(s);
        }
        return sb.toString();
    }
}