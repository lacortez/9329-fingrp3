package server;

import hangmangame.Hangman;
import hangmangame.HangmanHelper;
import org.omg.CosNaming.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;
import serverModels.HangmanImpl;

import javax.xml.stream.events.Characters;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class Server {

    public static Map<String, List<String>> userUsedWords = new HashMap<>();
    public static Map<String, String> userCurrWord  = new HashMap<>();
    public static Map<String, Integer> userMistakes  = new HashMap<>();
    public static Map<String, List<Character>> userInputChar  = new HashMap<>();

    public static void main(String[] args) {
        try{
            String[] param = readConfig();
            ORB orb;
            if(args.length > 1)
                orb = ORB.init(args, null);
            else
                orb = ORB.init(param, null);

            POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            rootpoa.the_POAManager().activate();

            //Servant
            HangmanImpl hangManImpl = new HangmanImpl();

            org.omg.CORBA.Object ref = rootpoa.servant_to_reference(hangManImpl);
            Hangman href = HangmanHelper.narrow(ref);

            org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");

            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            String name = "hangmanGame";
            NameComponent path[] = ncRef.to_name(name);
            ncRef.rebind(path, href);

            ServerController ctrl = new ServerController(hangManImpl);

            orb.run();
            ctrl.shutdownOrbListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    orb.shutdown(true);
                    orb.destroy();
                }
            });
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    private static String[] readConfig() throws IOException {
        StringBuilder cmd = new StringBuilder();
        String[] config = null;
        // read the configuration for the client
        FileInputStream file = new FileInputStream("../../../../config.properties");
        Properties pr = new Properties();
        pr.load(file);

        String host = pr.getProperty("host");
        String port = pr.getProperty("port");

        cmd.append(" -ORBInitial ").append(host);
        cmd.append(" -ORBInitialPort ").append(port);

        config = cmd.toString().split("\\s");
        return config;
    }

    public static List<String> setWord(String username, String word){
        List<String> words = new ArrayList<>();
        if (!userUsedWords.containsKey(username)) {
            words.add(word);
            return words;
        }
        words.addAll(userUsedWords.get(username));
        words.add(word);
        return words;
    }

    public static List<Character> setUsedChar(String username, char letter){
        List<Character> words = new ArrayList<>();
        if (!userInputChar.containsKey(username)) {
            words.add(letter);
            return words;
        }
        words.addAll(userInputChar.get(username));
        words.add(letter);
        return words;
    }

}
