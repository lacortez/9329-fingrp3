package server;

import serverModels.DbUtils;
import serverModels.HangmanImpl;
import serverViews.DatabaseConnect;
import serverViews.ServerView;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ServerController {
    private HangmanImpl servant;
    private DbUtils dbUtil = new DbUtils();
    private ServerView sVeiw;
    private DatabaseConnect dbConnectView;

    public ServerController(HangmanImpl servant) {
        this.servant = servant;
        sVeiw = new ServerView();
        sVeiw.typeInTxtArea("server.Server is Running...");
        dbConnectView = new DatabaseConnect();

        connectButtonFunction();
    }

    public void connectButtonFunction() {
        dbConnectView.setDbSetupButtonListener(event -> {
            try{
                dbUtil.setUser(dbConnectView.getdbUname());
                dbUtil.setPass(dbUtil.psToString(dbConnectView.getdbPass()));

                dbUtil.connectToDb();
                servant.setDbUtil(dbUtil);

                dbConnectView.close();
                sVeiw.typeInTxtArea("server.Server can now connect to Database...");
            } catch (Exception e) {
//                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Can't connect to database." +
                        "Incorrect username or password", "Connection Error", JOptionPane.ERROR_MESSAGE);
                connectButtonFunction();
            }
        });
    }

    public void shutdownOrbListener(ActionListener e){
        sVeiw.setShutDownButtonListener(e);
    }
}
