package serverViews;

import javax.swing.*;
import java.awt.event.ActionListener;

public class DatabaseConnect {
    private JTextField dbUnameTxtField;
    private JPasswordField dbPassField;
    private JButton dbSetupButton;
    private JPanel dbPanel;

    //public static void main(String[] args){new DatabaseConnect();}
    private JFrame dbFrame = new JFrame("Connect to MySQL Database");

    public DatabaseConnect () {
        dbFrame.setSize(600, 250);
        dbFrame.setContentPane(dbPanel);
        dbFrame.setLocationRelativeTo(null);
        dbFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        dbFrame.setVisible(true);
    }

    public void setDbSetupButtonListener(ActionListener aListener) {
        dbSetupButton.addActionListener(aListener);
    }

    public String getdbUname() {
        return dbUnameTxtField.getText();
    }

    public char[] getdbPass() {
        return dbPassField.getPassword();
    }

    public void close(){
        dbFrame.dispose();
    }
}
