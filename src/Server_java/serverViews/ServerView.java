package serverViews;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ServerView {
    private JPanel serverPanel;
    private JTextField ipTextField;
    private JTextArea serverTextArea;
    private JButton shutDownButton;

    //public static void main(String[] args){new ServerView();}

    public ServerView() {
        JFrame serverFrame = new JFrame("server.Server");
        serverFrame.setSize(600,575);

        serverFrame.setContentPane(serverPanel);
        serverFrame.setLocationRelativeTo(null);
        serverFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        ipTextField.setEditable(false);
        serverTextArea.setEditable(false);

        serverFrame.setVisible(true);
    }

    public void typeInTxtArea(String line) {
        serverTextArea.append(line + "\n");
    }

    public void setShutDownButtonListener(ActionListener aListener) {
        shutDownButton.addActionListener(aListener);
    }
}
